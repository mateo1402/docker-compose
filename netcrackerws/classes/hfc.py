#!/usr/bin/env python
# -*- coding: utf-8 -*-

class HfcClass():

    def getAllPortsByBuildingId(self, buildingId, technicalZone, assignmentStatus):
        mock_response = {'building' : {'buildingId' : buildingId, 'technicalZone': technicalZone, 'assignmentStatus' : assignmentStatus},
                        'splitter' : {'name':'DR12', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '90001', 'latitudeDd' : '44100', 'ncIdid' : 1341, 'creationDate': '20181104'},
                        'port': {'name' : 'DS10', 'physicalStatus' : 'available', 'physicalStatusChangeDate' : '20181203', 'assignmentStatus' : 'online', 'assignmentStatusChangeDate': '20191203', 'ncId' : '4124141', 'customerId': 41431, 'changeDate' : '20191212' }, 
                        'tap' : {'name' : 'TAP10', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '900011', 'latitudeDd' : '441001','ncId' : '41241410', 'customerId': 41431 }
        }
        return mock_response
    
    def getAllPortsByLocation(self, longitudeDd, latitudeDd, technicalZone, radius, assignmentStatus):
        mock_response = {'building' : {'buildingId' : 'AP12', 'technicalZone': technicalZone, 'assignmentStatus' : assignmentStatus},
                        'splitter' : {'name':'DR12', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '90001', 'latitudeDd' : '44100', 'ncId' : 1341, 'creationDate': '20181104'},
                        'port': {'name' : 'DS10', 'physicalStatus' : 'available', 'physicalStatusChangeDate' : '20181203', 'assignmentStatus' : 'online', 'assignmentStatusChangeDate': '20191203', 'ncId' : '4124141', 'customerId': 41431, 'changeDate' : '20191212' }, 
                        'tap' : {'name' : 'TAP10', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '900011', 'latitudeDd' : '441001','ncId' : '41241410', 'customerId': 41431 }
        }
        return mock_response

    def getAllPortsByTapId(self, tapId, assignmentStatus):
        mock_response = {'port': {'name' : 'DS10', 'physicalStatus' : 'available', 'physicalStatusChangeDate' : '20181203', 'assignmentStatus' : 'online', 'assignmentStatusChangeDate': '20191203', 'ncId' : '4124141', 'customerId': 41431, 'changeDate' : '20191212' }, 
                        'tap' : {'name' : 'TAP10', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '900011', 'latitudeDd' : '441001','ncId' : '41241410', 'customerId': 41431 }
        }
        return mock_response

    def getAllPortsByCustomerId(self, customerId):
        mock_response = {'splitter' : {'name':'DR12', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '90001', 'latitudeDd' : '44100', 'ncId' : 1341, 'creationDate': '20181104'},
                        'port': {'name' : 'DS10', 'physicalStatus' : 'available', 'physicalStatusChangeDate' : '20181203', 'assignmentStatus' : 'online', 'assignmentStatusChangeDate': '20191203', 'ncId' : '4124141', 'customerId': 41431, 'changeDate' : '20191212' }, 
                        'tap' : {'name' : 'TAP10', 'servedFloorsAndApartments' : '1 D', 'longitudeDd': '900011', 'latitudeDd' : '441001','ncId' : '41241410', 'customerId': 41431 }
        }
        return mock_response
    

    def reservePortByNcId(self, portNcId, customerId, transactionId):
        mock_response = {'transactionId' : '14133141','statusOperation' : 'reserved'}        
        return mock_response


    def assignPortByNcId(self, portNcId, customerId, transactionId):
        mock_response = {'transactionId' : '14133141','statusOperation' : 'assigned'}        
        return mock_response


    def releasePortByNcId(self, portNcId, transactionId):
        mock_response = {'transactionId' : '14133141','statusOperation' : 'released'}        
        return mock_response

    def portPendingDisconnectByNcId(self, portNcId, transactionId):
        mock_response = {'transactionId' : '14133141','statusOperation' : 'disconnected'}        
        return mock_response