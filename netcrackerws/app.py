from flask import Flask, render_template, flash, redirect, url_for, session, request, Response, jsonify
from classes.hfc import HfcClass 
from classes.gpon import GponClass
app = Flask(__name__)


# Index
@app.route('/')
def index():
    return render_template('home.html')

# HFC GetAllPortsByBuildingId
@app.route('/netcracker/hfcConnectivity/GetAllPortsByBuildingId/<buildingId>/<technicalZone>/<assignmentStatus>')
def hfcGetAllPortsByBuildingId(buildingId, technicalZone, assignmentStatus):
    hfc = HfcClass()
    response = hfc.getAllPortsByBuildingId(buildingId, technicalZone, assignmentStatus)
    return jsonify(response)
    
# GPON GetAllPortsByBuildingId
@app.route('/netcracker/gponConnectivity/GetAllPortsByBuildingId/<buildingId>/<technicalZone>/<assignmentStatus>')
def gponGetAllPortsByBuildingId(buildingId, technicalZone, assignmentStatus):
    gpon = GponClass()
    response = gpon.getAllPortsByBuildingId(buildingId, technicalZone, assignmentStatus)
    return jsonify(response)
    
# HFC GetAllPortsByLocation
@app.route('/netcracker/hfcConnectivity/GetAllPortsByLocation/<longitudeDd>/<latitudeDd>/<technicalZone>/<radius>/<assignmentStatus>')
def hfcGetAllPortsByLocation(longitudeDd, latitudeDd, technicalZone, radius, assignmentStatus):
    hfc = HfcClass()
    response = hfc.getAllPortsByLocation(longitudeDd, latitudeDd, technicalZone, radius, assignmentStatus)
    return jsonify(response)
    
# GPON GetAllPortsByLocation
@app.route('/netcracker/gponConnectivity/GetAllPortsByLocation/<longitudeDd>/<latitudeDd>/<technicalZone>/<radius>/<assignmentStatus>')
def gponGetAllPortsByLocation(longitudeDd, latitudeDd, technicalZone, radius, assignmentStatus):
    gpon = GponClass()
    response = gpon.getAllPortsByLocation(longitudeDd, latitudeDd, technicalZone, radius, assignmentStatus)
    return jsonify(response)
    
# GPON GetAllPortsByOpticalSplitterId
@app.route('/netcracker/gponConnectivity/GetAllPortsByOpticalSplitterId/<splitterId>/<assignmentStatus>')
def gponGetAllPortsByOpticalSplitterId(splitterId, assignmentStatus):
    gpon = GponClass()
    response = gpon.getAllPortsByOpticalSplitterId(splitterId, assignmentStatus)
    return jsonify(response)

# HFC GetAllPortsByTapId
@app.route('/netcracker/hfcConnectivity/GetAllPortsByTapId/<tapId>/<assignmentStatus>')
def hfcGetAllPortsByTapId(tapId, assignmentStatus):
    hfc = HfcClass()
    response = hfc.getAllPortsByTapId(tapId, assignmentStatus)
    return jsonify(response)

# HFC GetAllPortsByCustomerId
@app.route('/netcracker/hfcConnectivity/GetAllPortsByCustomerId/<customerId>')
def hfcGetAllPortsByCustomerId(customerId):
    hfc = HfcClass()
    response = hfc.getAllPortsByCustomerId(customerId)
    return jsonify(response)
    
# GPON GetAllPortsByCustomerId
@app.route('/netcracker/gponConnectivity/GetAllPortsByCustomerId/<customerId>')
def gponGetAllPortsByCustomerId(customerId):
    gpon = GponClass()
    response = gpon.getAllPortsByCustomerId(customerId)
    return jsonify(response)


# HFC ReservePortByNcId
@app.route('/netcracker/hfcConnectivity/ReservePortByNcId/<portNcId>/<customerId>/<transactionId>')
def hfcReservePortByNcId(portNcId, customerId, transactionId):
    hfc = HfcClass()
    response = hfc.reservePortByNcId(portNcId, customerId, transactionId)
    return jsonify(response)
    
# GPON ReservePortByNcId
@app.route('/netcracker/gponConnectivity/ReservePortByNcId/<portNcId>/<customerId>/<transactionId>')
def gponReservePortByNcId(portNcId, customerId, transactionId):
    gpon = GponClass()
    response = gpon.reservePortByNcId(portNcId, customerId, transactionId)
    return jsonify(response)

# HFC AssignPortByNcId
@app.route('/netcracker/hfcConnectivity/AssignPortByNcId/<portNcId>/<customerId>/<transactionId>')
def hfcAssignPortByNcId(portNcId, customerId, transactionId):
    hfc = HfcClass()
    response = hfc.assignPortByNcId(portNcId, customerId, transactionId)
    return jsonify(response)
    
# GPON AssignPortByNcId
@app.route('/netcracker/gponConnectivity/AssignPortByNcId/<portNcId>/<customerId>/<transactionId>')
def gponAssignPortByNcId(portNcId, customerId, transactionId):
    gpon = GponClass()
    response = gpon.assignPortByNcId(portNcId, customerId, transactionId)
    return jsonify(response)

# HFC ReleasePortByNcId
@app.route('/netcracker/hfcConnectivity/ReleasePortByNcId/<portNcId>/<transactionId>')
def hfcReleasePortByNcId(portNcId, transactionId):
    hfc = HfcClass()
    response = hfc.releasePortByNcId(portNcId, transactionId)
    return jsonify(response)
    
# GPON ReleasePortByNcId
@app.route('/netcracker/gponConnectivity/ReleasePortByNcId/<portNcId>/<transactionId>')
def gponReleasePortByNcId(portNcId, transactionId):
    gpon = GponClass()
    response = gpon.releasePortByNcId(portNcId, transactionId)
    return jsonify(response)

# HFC PortPendingDisconnectByNcId
@app.route('/netcracker/hfcConnectivity/PortPendingDisconnectByNcId/<portNcId>/<transactionId>')
def hfcPortPendingDisconnectByNcId(portNcId, transactionId):
    hfc = HfcClass()
    response = hfc.portPendingDisconnectByNcId(portNcId, transactionId)
    return jsonify(response)
    
# GPON PortPendingDisconnectByNcId
@app.route('/netcracker/gponConnectivity/PortPendingDisconnectByNcId/<portNcId>/<transactionId>')
def gponPortPendingDisconnectByNcId(portNcId, transactionId):
    gpon = GponClass()
    response = gpon.portPendingDisconnectByNcId(portNcId, transactionId)
    return jsonify(response)
    
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')

