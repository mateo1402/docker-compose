import time , logging , configparser
from flask import Flask, render_template, flash, redirect, url_for, session, request, Response
#from data import Articles
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps

# Read config from file
config = configparser.SafeConfigParser()
config.read('app.ini')

#Esta conf es para ver de forma general el log de la libreria.
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
#This is the right way of setting logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('hub.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)

app = Flask(__name__)

# Config MySQL
app.config['MYSQL_HOST'] = config.get('Db','Host')
app.config['MYSQL_USER'] = config.get('Db','Usuario')
app.config['MYSQL_PASSWORD'] = config.get('Db','Contrasena')
app.config['MYSQL_PORT'] = int(config.get('Db','Port'))
app.config['MYSQL_DB'] = config.get('Db','Db')
app.config['MYSQL_CURSORCLASS'] = config.get('Db','CURSORCLASS')
# init MYSQL
mysql = MySQL(app)

#Articles = Articles()

# Index
@app.route('/')
def index():
    return render_template('home.html')

# About
@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/hub')
def hfc():
     # Create cursor
    cur = mysql.connection.cursor()

    # CMTS
    result = cur.execute("SELECT  id cmts_id, LEFT(TRIM(description), 2) hub_name, type, SUBSTRING(interface,LOCATE('CABLE', interface)+5, LENGTH(interface)) interface  FROM cmts where is_used = 0")
    cmts = cur.fetchall()

    cmts_info = {}

    for cm in cmts:
        cmts_info[cm['hub_name']] = {}
        cmts_info[cm['hub_name']]['UP'] = []
        cmts_info[cm['hub_name']]['DW'] = []

    logging.debug(cmts_info)

    for cm in cmts:
        cmts_info[cm['hub_name']][cm['type']].append(cm['interface'])

    logging.error(cmts_info['AP']['UP'])
        
    #return Response(generate(cmts_info))

    info_html = {'cmts': cmts_info}
    
    return render_template('hfc-template.html', data=info_html)

# Articles
@app.route('/filter_cmts', methods=['GET', 'POST'])
def filter_cmts():

    hub_name   = request.json['hub_name']

    cur = mysql.connection.cursor()

    result  = cur.execute("select distinct(description) description from cmts where LEFT(TRIM(description), 2) = '" + str(hub_name) + "'" )
    cmts    = cur.fetchall()
    
    return render_template('cmts-selector.html', data=cmts)

# Articles
@app.route('/filter_type', methods=['GET', 'POST'])
def filter_type():

    cmts_name   = request.json['cmts_name']

    cur = mysql.connection.cursor()

    result  = cur.execute("select distinct(type) type from cmts where description = '" + str(cmts_name) + "'" )
    cmts    = cur.fetchall()
    
    return render_template('type_selector.html', data=cmts)


# Articles
@app.route('/filter_interface', methods=['GET', 'POST'])
def filter_interface():

    cmts_name        = request.json['cmts_name']
    type_interface  = request.json['type']

    cur = mysql.connection.cursor()

    result = cur.execute("SELECT  id cmts_id, SUBSTRING(interface,LOCATE('CABLE', interface)+5, LENGTH(interface)) interface  FROM cmts where is_used = 0 and description = '" + str(cmts_name) + "' and type = '"+str(type_interface)+"'")
    cmts = cur.fetchall()

    return render_template('type_interface.html', data=cmts)

# Articles
@app.route('/filter_prisma', methods=['GET', 'POST'])
def filter_prisma():

    hub_name        = request.json['hub_name'].strip()
    type_interface  = request.json['type_interface'].strip()
    
    prisma_interface = 'RX' 
    if (type_interface == 'UP'):
        prisma_interface = 'TX'
    
    cur = mysql.connection.cursor()

    result = cur.execute("SELECT p.id prisma_id, CONCAT(trim(chasis_id), '/', trim(slot_id), '/', trim(module_name)) prisma_desc from prisma p join prisma_location l ON l.ip_address = p.ip  where is_used = 0 and p.tx_rx = '"+prisma_interface+"' and l.location = '" +hub_name+ "'")
    prisma = cur.fetchall()

    return render_template('prisma_selector.html', data=prisma)


# Articles
@app.route('/filter_fibernodes', methods=['GET', 'POST'])
def filter_fibernodes():

    cmts_name           = request.json['cmts_name'].strip()
    type_interface      = request.json['type'].strip()

    cur = mysql.connection.cursor()

    result = cur.execute("select distinct(fn.name) description from nodo_operaciones no join fibernodes fn ON left(no.portName, 4) = fn.name and fn.is_used = 0 and cmts = '" + cmts_name +"'")
    fn = cur.fetchall()

    return render_template('fiber_nodes_selector.html', data=fn)

# Articles
@app.route('/articles')
def articles():
    # Create cursor
    cur = mysql.connection.cursor()

    # Get articles
    result = cur.execute("SELECT * FROM articles")

    articles = cur.fetchall()

    if result > 0:
        return render_template('articles.html', articles=articles)
    else:
        msg = 'No Articles Found'
        return render_template('articles.html', msg=msg)
    # Close connection
    cur.close()


#Single Article
@app.route('/article/<string:id>/')
def article(id):
    # Create cursor
    cur = mysql.connection.cursor()

    # Get article
    result = cur.execute("SELECT * FROM articles WHERE id = %s", [id])

    article = cur.fetchone()

    return render_template('article.html', article=article)


# Register Form Class
class RegisterForm(Form):
    name = StringField('Name', [validators.Length(min=1, max=50)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm Password')


# User Register
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))

        # Create cursor
        cur = mysql.connection.cursor()

        # Execute query
        cur.execute("INSERT INTO users(name, email, username, password) VALUES(%s, %s, %s, %s)", (name, email, username, password))

        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()

        flash('You are now registered and can log in', 'success')

        return redirect(url_for('login'))
    return render_template('register.html', form=form)


# User login
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Get Form Fields
        username = request.form['username']
        password_candidate = request.form['password']

        # Create cursor
        cur = mysql.connection.cursor()

        # Get user by username
        result = cur.execute("SELECT * FROM users WHERE username = %s", [username])

        if result > 0:
            # Get stored hash
            data = cur.fetchone()
            password = data['password']

            # Compare Passwords
            if sha256_crypt.verify(password_candidate, password):
                # Passed
                session['logged_in'] = True
                session['username'] = username

                flash('You are now logged in', 'success')
                return redirect(url_for('dashboard'))
            else:
                error = 'Invalid login'
                return render_template('login.html', error=error)
            # Close connection
            cur.close()
        else:
            error = 'Username not found'
            return render_template('login.html', error=error)

    return render_template('login.html')

# Check if user logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap

    
def generate(cmts_info):
    for key in cmts_info:
        for value in cmts_info[key]:
            yield '\t%s, %s: %s' % (cmts_info, value, cmts_info[key][value])


# Logout
@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash('You are now logged out', 'success')
    return redirect(url_for('login'))

# Dashboard
@app.route('/dashboard')
@is_logged_in
def dashboard():
    # Create cursor
    cur = mysql.connection.cursor()

    # Get articles
    #result = cur.execute("SELECT * FROM articles")
    # Show articles only from the user logged in 
    result = cur.execute("SELECT * FROM articles WHERE author = %s", [session['username']])

    articles = cur.fetchall()

    if result > 0:
        return render_template('dashboard.html', articles=articles)
    else:
        msg = 'No Articles Found'
        return render_template('dashboard.html', msg=msg)
    # Close connection
    cur.close()

# Article Form Class
class ArticleForm(Form):
    title = StringField('Title', [validators.Length(min=1, max=200)])
    body = TextAreaField('Body', [validators.Length(min=30)])

# Add Article
@app.route('/add_article', methods=['GET', 'POST'])
@is_logged_in
def add_article():
    form = ArticleForm(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        body = form.body.data

        # Create Cursor
        cur = mysql.connection.cursor()

        # Execute
        cur.execute("INSERT INTO articles(title, body, author) VALUES(%s, %s, %s)",(title, body, session['username']))

        # Commit to DB
        mysql.connection.commit()

        #Close connection
        cur.close()

        flash('Article Created', 'success')

        return redirect(url_for('dashboard'))

    return render_template('add_article.html', form=form)


# Edit Article
@app.route('/edit_article/<string:id>', methods=['GET', 'POST'])
@is_logged_in
def edit_article(id):
    # Create cursor
    cur = mysql.connection.cursor()

    # Get article by id
    result = cur.execute("SELECT * FROM articles WHERE id = %s", [id])

    article = cur.fetchone()
    cur.close()
    # Get form
    form = ArticleForm(request.form)

    # Populate article form fields
    form.title.data = article['title']
    form.body.data = article['body']

    if request.method == 'POST' and form.validate():
        title = request.form['title']
        body = request.form['body']

        # Create Cursor
        cur = mysql.connection.cursor()
        app.logger.info(title)
        # Execute
        cur.execute ("UPDATE articles SET title=%s, body=%s WHERE id=%s",(title, body, id))
        # Commit to DB
        mysql.connection.commit()

        #Close connection
        cur.close()

        flash('Article Updated', 'success')

        return redirect(url_for('dashboard'))

    return render_template('edit_article.html', form=form)

# Delete Article
@app.route('/delete_article/<string:id>', methods=['POST'])
@is_logged_in
def delete_article(id):
    # Create cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute("DELETE FROM articles WHERE id = %s", [id])

    # Commit to DB
    mysql.connection.commit()

    #Close connection
    cur.close()

    flash('Article Deleted', 'success')

    return redirect(url_for('dashboard'))

if __name__ == '__main__':
    app.secret_key='secret123'
    #Debug(app)
    app.run(host='0.0.0.0', port=80,debug=True)
